import * as Database from "cypher-query-builder";
import Noe4j from "neo4j-driver";
import Path from "path";
import Faker from "faker";
import {v4 as UuidV4} from "uuid";
import {endsWith, node, not, relation} from "cypher-query-builder";
import writeJson, {JsonType} from "./lib/write-json";

const likes = [
    "Manger",
    "Jouer",
    "Lire",
    "Regarder un film",
    "Regarder une série",
];

(async () => {
    const database = new Database.Connection("bolt://localhost",{
        username: "root",
        password: "root"
    },{
        // @ts-ignore
        driverConstructor: Noe4j.driver
    });

    await database.matchNode("n").detachDelete("n").run();

    const query_insert_likes = database.query();

    for(const item in likes){
        const date = (new Date()).toString();

        query_insert_likes.createNode("Like"+item,"Like",{
            content: likes[item],
            created_at: date,
            updated_at: date,
        })
    }

    await query_insert_likes.run();

    let number = 20;

    const inserted: JsonType[] = [];
    const query = database.query();

    for(const item in likes){
        query.matchNode("Like"+item,"Like",{
            content: likes[item],
        })
    }

    while(number > 0){
        const date = (new Date()).toString();

        const item = {
            last_name: Faker.name.lastName(),
            first_name: Faker.name.firstName(),
            email: Faker.internet.email(),
            id: UuidV4(),
            created_at: date,
            updated_at: date,
        };
        const name = "User"+number;

        inserted.push(item);

        query.createNode(name,"User",item)
        for(const key in likes){
            if (Math.random() < 0.4){
                query.create([
                    node(name),
                    relation('out', [ 'like' ]),
                    node("Like"+key)
                ]);
            }else if (Math.random() < 0.4){
                query.create([
                    node(name),
                    relation('out', [ 'notLike' ]),
                    node("Like"+key)
                ]);
            }
        }

        number--;
    }

    await Promise.all([
        writeJson(Path.join(__dirname,"../result/inserted.json"),inserted),
        query.run(),
    ]);

    const promises: Promise<void>[] = [];

    promises.push((async () => {
        const select = await database.matchNode("user","User")
            .where({ 'user.email': endsWith('@gmail.com', false) })
            .return("user")
            .run();

        await writeJson(Path.join(__dirname,"../result/keeped.json"),select.map(item => item.user.properties));
    })());
    promises.push((async () => {
        const query_remove = await database.matchNode("user","User")
            .where({ 'user.email': not(endsWith('@gmail.com', false)) })
            .with( ["user", {["user {.*}"]:  "snapshoot"}])
            .detachDelete("user")
            .return("snapshoot");

        const remove = await query_remove.run();
        await writeJson(Path.join(__dirname,"../result/removed.json"),remove.map(item => item.snapshoot));
    })());

    await Promise.all(promises);

    process.exit(0);
})();