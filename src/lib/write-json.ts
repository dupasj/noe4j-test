import writeFile from "./write";

type JsonType = {[key: string]: string|null|number|boolean|JsonType}|JsonType[]

const writeJson = (path: string, content: JsonType) => {
	return writeFile(path, JSON.stringify(content,null,"\t"));
};

export {
	JsonType
}

export default writeJson;