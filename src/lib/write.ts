import * as Path from "path";
import * as fs from "fs";

const mkdir = (src): Promise<void> => {
	return new Promise<void>((resolve, reject) => {
		fs.mkdir(src, {
			recursive: true
		}, function (err) {
			if (err) {
				reject(err);
			}

			resolve();
		});
	});
};

const writeFile = (path: string, content: string) => {
	return new Promise<void>((resolve, reject) => {
		mkdir(Path.dirname(path)).then(() => {
			fs.writeFile(path, content, function (err: Error) {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	});
};

export default writeFile;